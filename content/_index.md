---
title: "This Website and It's Purpose"
date: 2022-10-20T14:00:00-06:00
---
### What this site is about
The purpose of this site is twofold:
1. A helpful place where I post things about me, my content, and technology.
2. A place for me to document my thoughts and experiences as I further my education in the IT field.

### How it works
I wrote this site using [Hugo](https://gohugo.io), I manage the files with [Gitlab](https://gitlab.com) and host the site using [Netlify.](https://www.netlify.com)

Hugo is a fantastic and simple to use static site generator, I write my pages in Markdown, then Hugo converts the Markdown to HTML (and CSS and other website stuff) and sets it all up for me, then I push the changes to my Gitlab.

After that, Netlify reads my Gitlab repository, and if there are changes, builds the site and publishes the updates. 

It all works pretty well, and is fairly simple.

Check out the [Posts](/posts/) section for the blog style posts I do, and check out the [Articles](/articles) section for Tech/IT/Help articles.
