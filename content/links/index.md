---
title: Links/Social
date: 2022-10-16T12:44:19-06:00
---
Here is where you can find all the links to my social media, as well as to my Youtube and Twitch channels


###  Linux/General
- [Linux Youtube](https://www.youtube.com/channel/UC7bZiPZtSjjGeRc7rUDSCeA) Inactive
- [Mastodon](https://mstdn.starnix.network/@tjctheman) Inactive
- [VLOG Channel](https://youtube.com/@tjctheman-vlogs)


### Gaming
- [Youtube](https://www.youtube.com/@tjctheman)
- [Twitch.tv](https://twitch.tv/tjctheman)
- [Twitch VOD Youtube](https://www.youtube.com/@tjctheman-vods)
- [Discord](https://discord.gg/usFR7GX6Xt)
- [Twitter](https://twitter.com/tjctheman) Inactive





